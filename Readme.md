# example2

## gulp
- browser-sync
- node-scss
    - autoprefixer
    - cssnano
    - stylelint

### development / production
- development
    - enable sourcemaps
- production
    - delete .map files
    - disable sourcemaps

## pre-commit hooks
- husky
    - lint-staged
        - stylelint(scss)
        - htmlhint
