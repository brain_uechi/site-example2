const gulp = require('gulp');
const browserSync = require('browser-sync').create();

process.env.NODE_ENV = 'development';
let useSourceMaps = true;
if (!process.env.NODE_ENV) process.env.NODE_ENV = 'development';

/**
 * env task
 */
gulp.task('env:dev', function (callback) {
  process.env.NODE_ENV = 'development';
  useSourceMaps = true;

  console.log('##############################\n' +
    '## NODE_ENV: development\n' +
    '## useSourceMap: ture\n' +
    '##############################');

  callback();
});
gulp.task('env:prod', function (callback) {
  process.env.NODE_ENV = 'production';
  useSourceMaps = false;

  console.log('##############################\n' +
    '## NODE_ENV: production\n' +
    '## useSourceMap: false\n' +
    '##############################');

  callback();
});

/**
 * browser-sync task
 */
gulp.task('serve', function (callback) {
  const config = require('./bs-config');
  browserSync.init(config, () => {
    callback();
  });
});

/**
 * css lint task
 */
gulp.task('css:lint', function () {
  const stylelint = require('gulp-stylelint');

  return gulp.src([
    'htdocs/**/*.scss',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('css:lint'),
  })
  .pipe(stylelint({
    failAfterError: true,
    reporters: [
      {
        formatter: 'string',
        console: true,
      },
    ],
  }));
});

/**
 * css task
 */
gulp.task('css', function () {
  const sass = require('gulp-sass');
  const cached = require('gulp-cached');
  const progeny = require('gulp-progeny');
  const postcss = require('gulp-postcss');
  const autoprefixer = require('autoprefixer');
  const cssnano = require('cssnano');
  const rename = require('gulp-rename');
  const through = require('through2');

  return gulp.src([
    'htdocs/**/*.scss',
  ], {
    base: 'htdocs',
    sourcemaps: useSourceMaps,
  })
  .pipe(cached('scss'))
  .pipe(progeny())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss([
    autoprefixer({grid: true}),
    cssnano(),
  ]))
  .pipe(through.obj(function (file, enc, cb) {
    // append-buffer 対策
    file.contents = Buffer.from(file.contents.toString() + '\n');
    this.push(file);
    cb();
  }))
  .pipe(rename({extname: '.min.css'}))
  .pipe(gulp.dest('htdocs', {
    sourcemaps: '.',
  }))
  .pipe(browserSync.stream());
});

/**
 * css watch task
 */
gulp.task('css:watch', function (callback) {
  gulp.watch([
    'htdocs/**/*.scss',
  ], gulp.series('css:lint', 'css'));
  callback();
});

/**
 * build clean task
 */
gulp.task('build:clean', function (callback) {
  const path = require('path');
  const glob = require('glob');
  const fs = require('fs-extra');
  const files = glob.sync('htdocs/**/*.map', {nodir: true});
  files.forEach((file) => {
    let p = path.resolve(file);
    fs.removeSync(p);
    console.log(`delete ${p}`);
  });
  callback();
});

/**
 * task presets
 */
gulp.task('default', gulp.series('env:dev', 'css:watch', 'css:lint', 'css', 'serve'));
gulp.task('default-prod', gulp.series('env:prod', 'build:clean', 'css:watch', 'css:lint', 'css', 'serve'));
gulp.task('build', gulp.series('env:prod', 'build:clean', 'css:lint', 'css'));
